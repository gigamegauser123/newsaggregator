﻿namespace NewsAggregator.BLL.DTOs;

public class ArticleDto
{
    public Guid Id { get; set; }
    public string Title { get; set; } = null!;
    public string HtmlBody { get; set; } = null!;
    public string Author { get; set; } = null!;
    public Guid PublicId { get; set; }
    public DateTime Date { get; set; }
    public NewsWebsiteDto Source { get; set; }
    public TopicDto Topic { get; set; }
    public string SourceUrl { get; set; } = null!;
}