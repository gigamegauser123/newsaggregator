﻿namespace NewsAggregator.BLL.DTOs;

public class ArticlePreviewDto
{
    public Guid Id { get; set; }
    public Guid ArticleId { get; set; }
    public string Title { get; set; } = null!;
    public string? ImageUrl { get; set; }
    public string? PreviewText { get; set; } = null!;
    public Guid PublicArticleId { get; set; }
    public TopicDto? Topic { get; set; }
    public string ArticleUrl { get; set; } = null!;
    public DateTime Date { get; set; }
}
