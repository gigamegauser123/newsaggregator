﻿namespace NewsAggregator.BLL.DTOs;

public class NewsWebsiteDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string BaseUrl { get; set; } = null!;
}