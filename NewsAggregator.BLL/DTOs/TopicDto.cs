﻿namespace NewsAggregator.BLL.DTOs;

public class TopicDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string LocalizedName { get; set; } = null!;
}