﻿using AutoMapper;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.DAL.Entities;

namespace NewsAggregator.BLL.Mappings;

public class ArticleMappingProfile : Profile
{
    public ArticleMappingProfile()
    {
        // CreateMap<Article, ArticleDto>()
        //     .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
        //     .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
        //     .ForMember(dest => dest.Preview, opt => opt.MapFrom(src => src.Preview))
        //     .ForMember(dest => dest.HtmlBody, opt => opt.MapFrom(src => src.HtmlBody))
        //     .ForMember(dest => dest.Author, opt => opt.MapFrom(src => src.Author))
        //     .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
        //     .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.NewsWebsite))
        //     .ForMember(dest => dest.Topic, opt => opt.MapFrom(src => src.Topic))
        //     .ForMember(dest => dest.SourceUrl, opt => opt.MapFrom(src => src.SourceUrl));

        CreateMap<ArticleDto, Article>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
            .ForMember(dest => dest.HtmlBody, opt => opt.MapFrom(src => src.HtmlBody))
            .ForMember(dest => dest.Author, opt => opt.MapFrom(src => src.Author))
            .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date))
            .ForMember(dest => dest.NewsWebsite, opt => opt.MapFrom(src => src.Source.Id))
            .ForMember(dest => dest.PublicId, opt => opt.MapFrom(src => src.PublicId))
            .ForMember(dest => dest.Topic, opt => opt.MapFrom(src => src.Topic.Id))
            .ForMember(dest => dest.SourceUrl, opt => opt.MapFrom(src => src.SourceUrl));
    }
}