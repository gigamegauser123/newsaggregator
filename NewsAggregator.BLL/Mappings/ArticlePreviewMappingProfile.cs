﻿using AutoMapper;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.BLL.Mappings;

public class ArticlePreviewMappingProfile : Profile
{
    public ArticlePreviewMappingProfile()
    {
        CreateMap<ArticlePreviewDto, ArticlePreview>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.ArticleId, opt => opt.MapFrom(src => src.ArticleId))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
            .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
            .ForMember(dest => dest.PreviewText, opt => opt.MapFrom(src => src.PreviewText))
            .ForMember(dest => dest.Topic, opt => opt.MapFrom(src => src.Topic.Id))
            .ForMember(dest => dest.ArticleUrl, opt => opt.MapFrom(src => src.ArticleUrl))
            .ForMember(dest => dest.PublicArticleId, opt => opt.MapFrom(src => src.PublicArticleId))
            .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date));
        
    }
    
}