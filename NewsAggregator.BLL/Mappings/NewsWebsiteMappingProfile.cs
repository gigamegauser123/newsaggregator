﻿using AutoMapper;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.DAL.Entities;

namespace NewsAggregator.BLL.Mappings;

public class NewsWebsiteMappingProfile : Profile
{
    public NewsWebsiteMappingProfile()
    {
        CreateMap<NewsWebsite, NewsWebsiteDto>().ReverseMap();
    }
}