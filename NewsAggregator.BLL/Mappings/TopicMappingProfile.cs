﻿using AutoMapper;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.DAL.Entities;

namespace NewsAggregator.BLL.Mappings;

public class TopicMappingProfile : Profile
{
    public TopicMappingProfile()
    {
        CreateMap<Topic, TopicDto>().ReverseMap();
    }
}