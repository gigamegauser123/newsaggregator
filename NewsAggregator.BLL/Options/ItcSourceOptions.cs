﻿namespace NewsAggregator.BLL.Options;

public class ItcSourceOptions
{
    public string BaseUrl { get; set; } = null!;
    public string SourceName { get; set; } = null!;
    public DateTime StartParsingDate { get; set; } 
}