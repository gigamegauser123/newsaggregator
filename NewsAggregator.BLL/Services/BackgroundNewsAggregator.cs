﻿using System.Timers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Interfaces;
using NewsAggregator.DAL.Repositories.Interfaces;
using Timer = System.Timers.Timer;

namespace NewsAggregator.BLL.Services;

public class BackgroundNewsAggregator : IHostedService, IDisposable
{
    private readonly IServiceScopeFactory _scopeFactory;
    private Timer _timer;
    private readonly IConfiguration _configuration;

    public BackgroundNewsAggregator(IConfiguration configuration, IServiceScopeFactory scopeFactory)
    {
        _configuration = configuration;
        _scopeFactory = scopeFactory;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await DoWorkAsync(null, null, cancellationToken);
        _timer = new Timer();
        var aggregationPeriod = Convert.ToInt32(_configuration["NewsAggregationTimePeriod"]);
        _timer = new Timer(TimeSpan.FromSeconds(aggregationPeriod).TotalMilliseconds);
        _timer.Elapsed += async (sender, e) => await DoWorkAsync(sender, e, cancellationToken);
        _timer.Start();

    }

    private async Task DoWorkAsync(object sender, ElapsedEventArgs e, CancellationToken cancellationToken)
    {
        using (var scope = _scopeFactory.CreateScope())
        {
            var contentAggregator = scope.ServiceProvider.GetRequiredService<IContentAggregator>();
            await contentAggregator.AggregateAsync(cancellationToken);
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _timer?.Stop();
        _timer?.Dispose();

        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
    }
}