﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Options;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Interfaces;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.BLL.Services;

public class ContentAggregator : IContentAggregator
{
    private readonly IEnumerable<INewsSource> _newsSources;
    private readonly IArticleRepository _articleRepository;
    private readonly IArticlePreviewRepository _articlePreviewRepository;
    private readonly ITopicRepository _topicRepository;
    private readonly INewsWebsiteRepository _newsWebsiteRepository;
    private readonly ITopicNewsWebsiteRepository _topicNewsWebsiteRepository;
    private readonly IUnitOfWork<NewsAggregatorDbContext> _unitOfWork;
    private readonly IMapper _mapper;
    private readonly UnianSourceOptions _unianSourceOptions;
    private readonly LigaNetSourceOptions _ligaNetSourceOptions;
    private readonly ItcSourceOptions _itcSourceOptions;
    private readonly ILogger<ContentAggregator> _logger;

    public ContentAggregator(
        IEnumerable<INewsSource> newsSources,
        IArticleRepository articleRepository,
        IArticlePreviewRepository articlePreviewRepository,
        ITopicRepository topicRepository,
        INewsWebsiteRepository newsWebsiteRepository,
        ITopicNewsWebsiteRepository topicNewsWebsiteRepository,
        IUnitOfWork<NewsAggregatorDbContext> unitOfWork,
        IOptions<UnianSourceOptions> unianSourceOptions,
        IOptions<LigaNetSourceOptions> ligaNetSourceOptions,
        IOptions<ItcSourceOptions> itcSourceOption,
        IMapper mapper, ILogger<ContentAggregator> logger)
    {
        _newsSources = newsSources;
        _articleRepository = articleRepository;
        _articlePreviewRepository = articlePreviewRepository;
        _topicRepository = topicRepository;
        _newsWebsiteRepository = newsWebsiteRepository;
        _topicNewsWebsiteRepository = topicNewsWebsiteRepository;
        _unitOfWork = unitOfWork;
        _unianSourceOptions = unianSourceOptions.Value;
        _ligaNetSourceOptions = ligaNetSourceOptions.Value;
        _itcSourceOptions = itcSourceOption.Value;
        _mapper = mapper;
        _logger = logger;
    }

    public async Task AggregateAsync(CancellationToken cancellationToken)
    {
        try
        {
            var topics = (await _topicRepository.GetAllAsync(cancellationToken)).ToList();
            foreach (var newsSource in _newsSources)
            {
                var newsWebsite =
                    await _newsWebsiteRepository.SingleOrDefaultAsync(n => n.Name == newsSource.Name,
                        cancellationToken);
                
                if(newsWebsite is null) continue;
                
                foreach (var topic in topics)
                {
                    if (await _topicNewsWebsiteRepository.SingleOrDefaultAsync(
                            t => t.TopicId == topic.Id && t.NewsWebsiteId == newsWebsite.Id, cancellationToken) is null)
                    {
                        continue;
                    }

                    var articlesByTopic =
                        await _articleRepository.FindAsync(a => a.NewsWebsite == newsWebsite.Id && a.Topic == topic.Id,
                            cancellationToken);

                    var dateFrom = newsWebsite.Name switch
                    {
                        "Unian" => _unianSourceOptions.StartParsingDate,
                        "Liga.net" => _ligaNetSourceOptions.StartParsingDate,
                        "ITC" => _itcSourceOptions.StartParsingDate,
                        _ => DateTime.Now.AddDays(-2)
                    };

                    var dateTo = DateTime.Now;

                    if (articlesByTopic is not null && articlesByTopic.Any())
                    {
                        dateFrom = articlesByTopic.Max(a => a.Date).ToLocalTime().AddSeconds(1);
                    }

                    var articlesAndPreviews = await newsSource.FetchArticlesAsync(_mapper.Map<TopicDto>(topic),
                        dateFrom,
                        dateTo, cancellationToken);

                    if (articlesAndPreviews is null || !articlesAndPreviews.Any()) continue;

                    var articles = articlesAndPreviews.Select(a => a.Item2);
                    var articlePreviews = articlesAndPreviews.Select(a => a.Item1);

                    var distinctArticles = articles.GroupBy(obj => obj.SourceUrl)
                        .Select(group => group.First())
                        .ToList();
                    
                    var distinctArticlePreviews = articlePreviews.GroupBy(obj => obj.ArticleUrl)
                        .Select(group => group.First())
                        .ToList();

                    
                    if (articles.Count() != distinctArticles.Count)
                    {
                        var a = 5;
                    }
                    if (articlePreviews.Count() != distinctArticlePreviews.Count)
                    {
                        var a = 5;
                    }
                    
                    
                    
                    await _articleRepository.AddRangeAsync(
                        _mapper.Map<IEnumerable<Article>>(distinctArticles), cancellationToken);
                    await _articlePreviewRepository.AddRangeAsync(
                        _mapper.Map<IEnumerable<ArticlePreview>>(distinctArticlePreviews), cancellationToken);
                }
            }

            await _unitOfWork.CommitAsync(cancellationToken);
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Aggregation error: {e.Message}");
        }
    }
}