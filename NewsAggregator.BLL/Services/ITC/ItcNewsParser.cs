﻿using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using AngleSharp;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Options;
using NewsAggregator.BLL.Services.ITC.Interfaces;
using NewsAggregator.BLL.Services.Utils;

namespace NewsAggregator.BLL.Services.ITC;

public class ItcNewsParser : IItcNewsParser
{
    private readonly IBrowsingContext _browsingContext;
    private readonly WebClient _webClient;
    private readonly ItcSourceOptions _sourceOptions;
    private readonly StringBuilder _stringBuilder;
    private readonly ILogger<ItcNewsParser> _logger;

    public ItcNewsParser(
        IBrowsingContext browsingContext,
        WebClient webClient,
        IOptions<ItcSourceOptions> sourceOptions, ILogger<ItcNewsParser> logger)
    {
        _browsingContext = browsingContext;
        _webClient = webClient;
        _logger = logger;
        _sourceOptions = sourceOptions.Value;
        _stringBuilder = new StringBuilder();
    }

    public async Task<List<ArticlePreviewDto>?> ParseArticlePreviewsAsync(
        string url,
        DateTime dateFrom,
        DateTime dateTo,
        CancellationToken cancellationToken)
    {
        try
        {
            var isFinal = false;
            var parsedArticlePreviews = new List<ArticlePreviewDto>();
            var pageCount = 1;

            while (!isFinal)
            {
                var page = await _browsingContext.OpenAsync(url + $"/page/{pageCount}", cancellationToken);
                var articlePreviewsContainer = page.QuerySelector("#content");
                if (articlePreviewsContainer is null) break;
                var articlePreviews = articlePreviewsContainer.QuerySelectorAll(".post");

                if (articlePreviews.Length > 1)
                {
                    for (var i = 0; i < articlePreviews.Length; i++)
                    {
                        var articleInfo = articlePreviews[i].QuerySelector(".row");
                        if (articleInfo is null) continue;
                        var articleTitle = articleInfo?.QuerySelector(".entry-title")?.TextContent.Replace("\n", "");
                        var articleUrl = articleInfo?.QuerySelector("a.thumb-responsive")?.Attributes
                            .GetNamedItem("href")
                            ?.Value;
                        var articleDate = articleInfo?.QuerySelector(".date")?.TextContent.Replace("\n", "");

                        var parsedArticleDate = DateParser.ParseDateTime(articleDate);

                        if (parsedArticleDate > dateTo) continue;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        var articleThumbnail = articleInfo.QuerySelector("a.thumb-responsive");

                        var imageUrl = articleThumbnail?.Attributes.GetNamedItem("data-bg")?.Value;

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            ImageUrl = imageUrl,
                            ArticleUrl = articleUrl,
                            Date = parsedArticleDate.ToUniversalTime()
                        });

                        _logger.LogInformation(
                            $"Parsed article preview in ItcNewsParser. Page {pageCount}, preview number {i})");
                    }

                    await Task.Delay(TimeSpan.FromMilliseconds(500), cancellationToken);
                    if (parsedArticlePreviews.Count < 10 && pageCount > 10) isFinal = true;
                    pageCount++;
                }
                else
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(2000), cancellationToken);
                }
            }

            return parsedArticlePreviews;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article previews error in ItcNewsParser: {e.Message}");
            return null;
        }
    }

    public async Task<ArticleDto?> ParseArticleAsync(string url, CancellationToken cancellationToken)
    {
        try
        {
            var page = await _browsingContext.OpenAsync(url);

            var articleContainer = page.QuerySelector("#wrapper .container");
            if (articleContainer is null) return null;

            var articleTitle = articleContainer.QuerySelector(".entry-title")?.TextContent.Replace("\n", "").Trim();
            var articleDate = articleContainer.QuerySelector(".date")?.TextContent.Trim();
            var parsedArticleDate = DateParser.ParseDateTime(articleDate);

            var articleContent = articleContainer.QuerySelector(".post-txt");
            if (articleContent is null) return null;
            var articleAuthor = articleContainer.QuerySelector(".author-right a")?.TextContent;
            var isFinal = false;


            foreach (var articleElement in articleContent.Children)
            {
                if (isFinal) break;

                switch (articleElement)
                {
                    case IHtmlParagraphElement paragraph:
                        if (paragraph.TextContent != string.Empty)
                        {
                            if (paragraph.TextContent.Contains("Джерело: "))
                            {
                                var linkElement = paragraph.QuerySelector("a");
                                if (linkElement != null)
                                {
                                    var linkUrl = linkElement.Attributes.GetNamedItem("href")?.Value;
                                    _stringBuilder.Append(
                                        $"<p>Джерело: <a href='{linkUrl}'>{linkElement.TextContent}</a></p>");
                                }

                                break;
                            }

                            _stringBuilder.Append($"<p>{paragraph.TextContent.Replace("\n", "")}</p>");
                        }

                        var imageElement = paragraph.QuerySelector("a img");
                        if (imageElement != null)
                        {
                            var paragraphImageUrl = imageElement?.Attributes.GetNamedItem("data-src")?.Value ??
                                                    imageElement?.Attributes.GetNamedItem("src")?.Value;
                            var paragraphImageAlt = imageElement?.Attributes.GetNamedItem("alt")?.Value;
                            var paragraphImageTitle = imageElement?.Attributes.GetNamedItem("title")?.Value;
                            _stringBuilder.Append(
                                $"<img alt='{paragraphImageAlt}' src='{paragraphImageUrl}' title='{paragraphImageTitle}'>");
                        }

                        break;
                    case IHtmlHeadingElement heading:
                        if (heading.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<{heading.LocalName}>{heading.TextContent}</{heading.LocalName}>");
                        }

                        break;

                    case IHtmlImageElement htmlImageElement:
                        var imageUrl = htmlImageElement?.Attributes.GetNamedItem("data-src")?.Value ??
                                       htmlImageElement?.Attributes.GetNamedItem("src")?.Value;
                        var imageAlt = htmlImageElement?.Attributes.GetNamedItem("alt")?.Value;
                        var imageTitle = htmlImageElement?.Attributes.GetNamedItem("title")?.Value;
                        _stringBuilder.Append($"<img alt='{imageAlt}' src='{imageUrl}' title='{imageTitle}'>");
                        break;

                    case IHtmlElement { ClassName: "quote_blue" } htmlQuoteElement:
                        var paragraphElement = htmlQuoteElement.QuerySelector("p");
                        if (paragraphElement is not null)
                        {
                            var text = paragraphElement.TextContent;
                            if (text != string.Empty)
                            {
                                _stringBuilder.Append($"<p>{text}</p>");
                            }
                        }

                        break;
                    case IHtmlUnorderedListElement unorderedListElement:
                        _stringBuilder.Append("<ul>");
                        foreach (var li in unorderedListElement.Children)
                        {
                            if (li is IHtmlListItemElement)
                            {
                                _stringBuilder.Append($"<li>{li.TextContent}</li>");
                            }
                        }

                        _stringBuilder.Append("</ul>");
                        break;
                }
            }

            var article = new ArticleDto
            {
                Title = articleTitle,
                HtmlBody = _stringBuilder.ToString(),
                Author = articleAuthor,
                Date = parsedArticleDate.ToUniversalTime(),
                SourceUrl = url
            };

            _logger.LogInformation($"Parsed article in ItcNewsParser. Article url: {url}");
            _stringBuilder.Clear();

            return article;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article error in ItcNewsParser (Article url: {url}) {e.Message}");
            return null;
        }
    }
}