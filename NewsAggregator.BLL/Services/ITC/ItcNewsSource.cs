﻿using AutoMapper;
using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Options;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.BLL.Services.ITC.Interfaces;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.BLL.Services.ITC;

public class ItcNewsSource<TNewsParser> : INewsSource
    where TNewsParser : IItcNewsParser
{
    public string Name { get; }
    public string Url { get; }
    private TNewsParser _newsParser;
    private readonly ItcSourceOptions _sourceOptions;
    private readonly NewsWebsiteDto _newsWebsiteDto;
    private readonly IArticlePreviewRepository _articlePreviewRepository;
    private readonly INewsWebsiteRepository _newsWebsiteRepository;
    private readonly ITopicNewsWebsiteRepository _topicNewsWebsiteRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<ItcNewsParser> _logger;

    public ItcNewsSource(
        IOptions<ItcSourceOptions> sourceOptions,
        TNewsParser newsParser,
        INewsWebsiteRepository newsWebsiteRepository,
        ITopicNewsWebsiteRepository topicNewsWebsiteRepository,
        IMapper mapper,
        IArticlePreviewRepository articlePreviewRepository, ILogger<ItcNewsParser> logger)
    {
        _sourceOptions = sourceOptions.Value;
        _newsParser = newsParser;
        Name = _sourceOptions.SourceName;
        Url = _sourceOptions.BaseUrl;
        _newsWebsiteRepository = newsWebsiteRepository;
        _topicNewsWebsiteRepository = topicNewsWebsiteRepository;
        _mapper = mapper;
        _articlePreviewRepository = articlePreviewRepository;
        _logger = logger;
        _newsWebsiteDto = _mapper.Map<NewsWebsiteDto>(_newsWebsiteRepository.SingleOrDefault(n => n.Name == Name));
    }

    public async Task<List<(ArticlePreviewDto, ArticleDto)>?> FetchArticlesAsync(TopicDto topic, DateTime dateFrom,
        DateTime dateTo, CancellationToken cancellationToken)
    {
        try
        {
            var result = new List<(ArticlePreviewDto, ArticleDto)>();
            var topicNewsWebsite =
                await _topicNewsWebsiteRepository.SingleOrDefaultAsync(
                    t => t.TopicId == topic.Id && t.NewsWebsiteId == _newsWebsiteDto.Id, cancellationToken);
            if (topicNewsWebsite is null) return new List<(ArticlePreviewDto, ArticleDto)>();

            var articlePreviews =
                await _newsParser.ParseArticlePreviewsAsync(topicNewsWebsite.Url, dateFrom, dateTo, cancellationToken);
            if (articlePreviews is null || !articlePreviews.Any()) return null;

            var newsSource = await _newsWebsiteRepository.SingleOrDefaultAsync(n => n.Name == Name, cancellationToken);
            var newsSourceDto = _mapper.Map<NewsWebsiteDto>(newsSource);

            var existingArticlePreviews = await _articlePreviewRepository.GetAllAsync(cancellationToken);
            if (existingArticlePreviews is not null || existingArticlePreviews.Any())
            {
                articlePreviews = articlePreviews.Where(article =>
                        !existingArticlePreviews.Any(
                            existingArticle => existingArticle.ArticleUrl == article.ArticleUrl))
                    .ToList();
            }

            foreach (var articlePreview in articlePreviews)
            {
                articlePreview.Topic = topic;
                var article = await _newsParser.ParseArticleAsync(articlePreview.ArticleUrl, cancellationToken);
                if (article is null) continue;
                article.Id = articlePreview.ArticleId;
                article.Topic = topic;
                article.Source = newsSourceDto;
                article.PublicId = Guid.NewGuid();
                articlePreview.PublicArticleId = article.PublicId;
                if (articlePreview.ImageUrl is null)
                {
                    var doc = new HtmlDocument();
                    doc.LoadHtml(article.HtmlBody);

                    var imgNode = doc.DocumentNode.SelectSingleNode("//img");
                    if (imgNode != null)
                    {
                        var imageUrl = imgNode.GetAttributeValue("src", "");
                        articlePreview.ImageUrl = imageUrl;
                    }
                    else
                    {
                        articlePreview.ImageUrl = "images/itc.jpg";
                    }
                }

                result.Add((articlePreview, article));
            }

            return result;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Fetching articles error in ItcNewsSource. Exception: {e.Message}");
            return null;
        }
    }
}