﻿namespace NewsAggregator.BLL.Services.Interfaces;

public interface IContentAggregator
{
    public Task AggregateAsync(CancellationToken cancellationToken);
}