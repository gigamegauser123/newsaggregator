﻿using NewsAggregator.BLL.DTOs;

namespace NewsAggregator.BLL.Services.Interfaces;

public interface INewsParser
{
    public Task<List<ArticlePreviewDto>?> ParseArticlePreviewsAsync(string url, DateTime dateFrom, DateTime dateTo, CancellationToken cancellationToken);
    public Task<ArticleDto?> ParseArticleAsync(string url, CancellationToken cancellationToken);
}