﻿using System.Linq.Expressions;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.DAL.Entities;

namespace NewsAggregator.BLL.Services.Interfaces;

public interface INewsService
{
    public Task<List<ArticlePreviewDto>> GetArticlePreviewsAsync(CancellationToken cancellationToken, string? topic = null, int pageNumber = 1, int pageSize = 20);
    public Task<List<ArticleDto>> GetArticleAsync(Guid articleId, CancellationToken cancellationToken);
    Task<int> GetRecordsCountAsync(CancellationToken cancellationToken, Expression<Func<ArticlePreview, bool>>? predicate = null);

    Task<TopicDto> GetTopicAsync(string topicName, CancellationToken cancellationToken);
    Task<ArticleDto?> GetArticleAsync(TopicDto topic, Guid publicArticleId, CancellationToken cancellationToken);
    Task<List<ArticlePreviewDto>> GetLastArticlePreviewAsync(int count, CancellationToken cancellationToken);
}