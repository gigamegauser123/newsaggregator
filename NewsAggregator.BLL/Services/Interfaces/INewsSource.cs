﻿using NewsAggregator.BLL.DTOs;

namespace NewsAggregator.BLL.Services.Interfaces;

public interface INewsSource
{
    public string Name { get; }
    public string Url { get; }
    public Task<List<(ArticlePreviewDto, ArticleDto)>?> FetchArticlesAsync(TopicDto topic, DateTime dateFrom, DateTime dateTo, CancellationToken cancellationToken);
}