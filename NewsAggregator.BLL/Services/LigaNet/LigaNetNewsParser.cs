﻿using System.Globalization;
using System.Net;
using System.Text;
using AngleSharp;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Services.LigaNet.Interfaces;
using NewsAggregator.BLL.Services.Utils;

namespace NewsAggregator.BLL.Services.LigaNet;

public class LigaNetNewsParser : ILigaNetNewsParser
{
    private readonly IBrowsingContext _browsingContext;
    private readonly WebClient _webClient;
    private readonly StringBuilder _stringBuilder;
    private readonly ILogger<LigaNetNewsParser> _logger;

    public LigaNetNewsParser(IBrowsingContext browsingContext, WebClient webClient, ILogger<LigaNetNewsParser> logger)
    {
        _browsingContext = browsingContext;
        _webClient = webClient;
        _logger = logger;
        _stringBuilder = new StringBuilder();
    }

    public async Task<List<ArticlePreviewDto>?> ParseArticlePreviewsAsync(string url, DateTime dateFrom,
        DateTime dateTo,
        CancellationToken cancellationToken)
    {
        try
        {
            var isFinal = false;
            var parsedArticlePreviews = new List<ArticlePreviewDto>();
            var pageCount = 1;

            while (!isFinal)
            {
                var page = await _browsingContext.OpenAsync(url + $"/page/{pageCount}", cancellationToken);

                var articlePreviews = page.QuerySelectorAll(".news-col");

                if (articlePreviews.Any())
                {
                    for (var i = 0; i < articlePreviews.Length; i++)
                    {
                        var articleInfo = articlePreviews[i].QuerySelector("ul li");
                        var articleTitle = articleInfo?.QuerySelector(".title")?.TextContent.Replace("\n", "");
                        if (articleTitle.ToLower().Contains("відео")) continue;

                        var articleUrl = articleInfo?.QuerySelector(".title")?.Attributes.GetNamedItem("href")?.Value;
                        if (articleUrl.ToLower().Contains("video")) continue;
                        var articleDate = articleInfo?.QuerySelector(".time")?.TextContent.Replace("\n", "").Trim();
                        if (articleDate is null) continue;

                        var parsedArticleDate = DateParser.ParseDateTime(articleDate);

                        if (parsedArticleDate > dateTo) continue;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            Date = parsedArticleDate.ToUniversalTime(),
                            ArticleUrl = articleUrl
                        });

                        _logger.LogInformation(
                            $"Parsed article preview in LigaNetNewsParser. Page {pageCount}, preview number {i})");
                    }

                    var articleWithImagePreviews = page.QuerySelectorAll(".with-photo");


                    for (var i = 0; i < articleWithImagePreviews.Length; i++)
                    {
                        var articleInfo = articleWithImagePreviews[i];
                        var articleTitle = articleInfo?.QuerySelector(".title a")?.TextContent.Replace("\n", "");
                        if (articleTitle.ToLower().Contains("відео")) continue;

                        var articleUrl = articleInfo?.QuerySelector(".title a")?.Attributes.GetNamedItem("href")?.Value;
                        if (articleUrl.ToLower().Contains("video")) continue;
                        var articleDate = articleInfo?.QuerySelector(".time")?.TextContent.Replace("\n", "").Trim();
                        if (articleDate is null) continue;

                        var parsedArticleDate = DateParser.ParseDateTime(articleDate);

                        if (parsedArticleDate > dateTo) continue;

                        var articleThumbnail = articleInfo.QuerySelector("img");
                        var imageUrl = articleThumbnail?.Attributes.GetNamedItem("data-src")?.Value;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            Date = parsedArticleDate.ToUniversalTime(),
                            ArticleUrl = articleUrl,
                            ImageUrl = imageUrl
                        });

                        _logger.LogInformation(
                            $"Parsed article preview in LigaNetNewsParser. Page {pageCount}, preview number {i})");
                    }
                }
                else if (page.QuerySelectorAll(".news li:not(.with-photo)").Any())
                {
                    var newsPreviews = page.QuerySelectorAll(".news li:not(.with-photo)");
                    for (var i = 0; i < newsPreviews.Length; i++)
                    {
                        var articleInfo = newsPreviews[i];
                        var articleTitle = articleInfo?.QuerySelector("a")?.TextContent.Replace("\n", "");
                        if (articleTitle.ToLower().Contains("відео")) continue;

                        var articleUrl = articleInfo?.QuerySelector("a")?.Attributes.GetNamedItem("href")?.Value;
                        if (articleUrl.ToLower().Contains("video")) continue;
                        var articleDate = articleInfo?.QuerySelector(".time")?.TextContent.Replace("\n", "").Trim();
                        if (articleDate is null) continue;

                        var parsedArticleDate = DateParser.ParseDateTime(articleDate);

                        if (parsedArticleDate > dateTo) continue;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            Date = parsedArticleDate.ToUniversalTime(),
                            ArticleUrl = articleUrl
                        });
                    }

                    var articleWithImagePreviews = page.QuerySelectorAll(".with-photo");


                    for (var i = 0; i < articleWithImagePreviews.Length; i++)
                    {
                        var articleInfo = articleWithImagePreviews[i];
                        var articleTitle = articleInfo?.QuerySelector("a:not(.image a)")?.TextContent.Replace("\n", "");
                        if (articleTitle.ToLower().Contains("відео")) continue;

                        var articleUrl = articleInfo?.QuerySelector("a:not(.image a)")?.Attributes.GetNamedItem("href")
                            ?.Value;
                        if (articleUrl.ToLower().Contains("video")) continue;
                        var articleDate = articleInfo?.QuerySelector(".time")?.TextContent.Replace("\n", "").Trim();

                        var parsedArticleDate = DateParser.ParseDateTime(articleDate);

                        if (parsedArticleDate > dateTo) continue;

                        var articleThumbnail = articleInfo.QuerySelector(".image img");
                        var imageUrl = articleThumbnail?.Attributes.GetNamedItem("data-src")?.Value;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            Date = parsedArticleDate.ToUniversalTime(),
                            ArticleUrl = articleUrl,
                            ImageUrl = imageUrl
                        });

                        _logger.LogInformation(
                            $"Parsed article preview in LigaNetNewsParser. Page {pageCount}, preview number {i})");
                    }
                }
                else
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(2000), cancellationToken);
                    continue;
                }

                await Task.Delay(TimeSpan.FromMilliseconds(500), cancellationToken);
                if (parsedArticlePreviews.Count < 10 && pageCount > 10) isFinal = true;
                pageCount++;
            }

            return parsedArticlePreviews;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article previews error in LigaNetNewsParser: {e.Message}");
            return null;
        }
    }

    public async Task<ArticleDto?> ParseArticleAsync(string url, CancellationToken cancellationToken)
    {
        try
        {
            var page = await _browsingContext.OpenAsync(url);

            var articleContainer = page.QuerySelector(".article-content");
            if (articleContainer is null) return null;

            var articleTitle = articleContainer.QuerySelector("h1").TextContent.Replace("\n", "");
            var articleDate = articleContainer.QuerySelector(".article-time")?.TextContent.Trim();
            var parsedArticleDate = DateParser.ParseDateTime(articleDate);

            var previewImage = articleContainer.QuerySelector(".col-12 img");
            if (previewImage is not null)
            {
                var imageUrl = previewImage.Attributes.GetNamedItem("src").Value;
                var imageAlt = previewImage.Attributes.GetNamedItem("alt").Value;
                var imageTitle = previewImage.Attributes.GetNamedItem("title").Value;
                _stringBuilder.Append($"<img alt='{imageAlt}' src='{imageUrl}' title='{imageTitle}'>");
            }

            var articleContent = articleContainer.QuerySelector("#news-text");
            if (articleContent is null) return null;
            var articleAuthor = articleContent.QuerySelector(".author-redactor")?.TextContent;
            var isFinal = false;


            foreach (var articleElement in articleContent.Children)
            {
                if (isFinal) break;

                switch (articleElement)
                {
                    case IHtmlParagraphElement paragraph:
                        if (paragraph.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<p>{paragraph.TextContent}</p>");
                        }

                        break;
                    case IHtmlHeadingElement heading:
                        if (heading.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<{heading.LocalName}>{heading.TextContent}</{heading.LocalName}>");
                        }

                        break;

                    case IHtmlElement { ClassName: "content-image" } htmlElement:
                        var image = htmlElement.QuerySelector("img");
                        var imageUrl = image?.Attributes.GetNamedItem("data-src")?.Value ??
                                       image?.Attributes.GetNamedItem("src")?.Value;
                        var imageAlt = image?.Attributes.GetNamedItem("alt")?.Value;
                        var imageTitle = image?.Attributes.GetNamedItem("title")?.Value;
                        _stringBuilder.Append($"<img alt='{imageAlt}' src='{imageUrl}' title='{imageTitle}'>");
                        break;
                    case IHtmlUnorderedListElement unorderedListElement:
                        _stringBuilder.Append("<ul>");
                        foreach (var li in unorderedListElement.Children)
                        {
                            if (li is IHtmlListItemElement)
                            {
                                _stringBuilder.Append($"<li>{li.TextContent}</li>");
                            }
                        }

                        _stringBuilder.Append("</ul>");
                        break;
                }
            }

            var article = new ArticleDto
            {
                Title = articleTitle,
                HtmlBody = _stringBuilder.ToString(),
                Author = articleAuthor,
                Date = parsedArticleDate.ToUniversalTime(),
                SourceUrl = url
            };

            _logger.LogInformation($"Parsed article in LigaNetNewsParser. Article url: {url}");

            _stringBuilder.Clear();

            return article;
        }

        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article error in LigaNetNewsParser (Article url: {url}) {e.Message}");
            return null;
        }
    }
}