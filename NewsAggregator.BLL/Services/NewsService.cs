﻿using System.Linq.Expressions;
using AutoMapper;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.BLL.Services;

public class NewsService : INewsService
{
    private readonly IArticlePreviewRepository _articlePreviewRepository;
    private readonly IArticleRepository _articleRepository;
    private readonly ITopicRepository _topicRepository;
    private readonly INewsWebsiteRepository _newsWebsiteRepository;
    private readonly IMapper _mapper;

    public NewsService(IArticlePreviewRepository articlePreviewRepository, IArticleRepository articleRepository, ITopicRepository topicRepository, IMapper mapper, INewsWebsiteRepository newsWebsiteRepository)
    {
        _articlePreviewRepository = articlePreviewRepository;
        _articleRepository = articleRepository;
        _topicRepository = topicRepository;
        _mapper = mapper;
        _newsWebsiteRepository = newsWebsiteRepository;
    }

    public async Task<List<ArticlePreviewDto>> GetArticlePreviewsAsync(CancellationToken cancellationToken, string? topic = null, int pageNumber = 1,
        int pageSize = 20)
    {
        var topicFromDb = await _topicRepository.SingleOrDefaultAsync(t => t.Name.ToLower() == topic.ToLower(), cancellationToken);
        if (topicFromDb is null) throw new ArgumentException($"Topic with name {topic} doesn't exist");
        
        var articlePreviews =
            await _articlePreviewRepository.GetPaginatedListAsync(pageSize, pageNumber, cancellationToken, topicFromDb);

        var articlePreviewsDtos = new List<ArticlePreviewDto>();
        var topics = (await _topicRepository.GetAllAsync(cancellationToken) ?? Array.Empty<Topic>()).ToList();
        foreach (var articlePreview in articlePreviews)
        {
            var articleTopic = topics.SingleOrDefault(t => t.Id == articlePreview.Topic);
            articlePreviewsDtos.Add(new ArticlePreviewDto
            {
                Id = articlePreview.Id,
                ArticleId = articlePreview.ArticleId,
                Title = articlePreview.Title,
                ImageUrl = articlePreview.ImageUrl,
                PreviewText = articlePreview.PreviewText,
                Date = articlePreview.Date.ToLocalTime(),
                ArticleUrl = articlePreview.ArticleUrl,
                PublicArticleId = articlePreview.PublicArticleId,
                Topic = _mapper.Map<TopicDto>(articleTopic)
            });
        }

        return articlePreviewsDtos;
    }

    public async Task<List<ArticleDto>> GetArticleAsync(Guid articleId, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public async Task<int> GetRecordsCountAsync(CancellationToken cancellationToken, Expression<Func<ArticlePreview, bool>>? predicate = null)
    {
        return await _articlePreviewRepository.GetRecordsCountAsync(cancellationToken, predicate);
    }

    public async Task<TopicDto> GetTopicAsync(string topicName, CancellationToken cancellationToken)
    {
        var topic = await _topicRepository.SingleOrDefaultAsync(t => t.Name.ToLower() == topicName.ToLower(), cancellationToken);
        return _mapper.Map<TopicDto>(topic);
    }

    public async Task<ArticleDto?> GetArticleAsync(TopicDto topic, Guid publicArticleId,
        CancellationToken cancellationToken)
    {
        var article =
            await _articleRepository.SingleOrDefaultAsync(a => a.Topic == topic.Id && a.PublicId == publicArticleId,
                cancellationToken);

        if (article != null)
        {
            var newsWebsite = await _newsWebsiteRepository.GetAsync(article.NewsWebsite, cancellationToken);
            
            var articleDto = new ArticleDto
            {
                Id = article.Id,
                Date = article.Date,
                Author = article.Author,
                HtmlBody = article.HtmlBody,
                PublicId = publicArticleId,
                SourceUrl = article.SourceUrl,
                Title = article.Title,
                Topic = topic
            };
        
            return articleDto;
        }

        return null;
    }

    public async Task<List<ArticlePreviewDto>> GetLastArticlePreviewAsync(int count, CancellationToken cancellationToken)
    {
        var lastArticlePreview = await _articlePreviewRepository.GetLastArticlePreviewAsync(count, cancellationToken);
        var articlePreviewsDtos = new List<ArticlePreviewDto>();
        
        var topics = (await _topicRepository.GetAllAsync(cancellationToken) ?? Array.Empty<Topic>()).ToList();

        foreach (var articlePreview in lastArticlePreview)
        {
            var articleTopic = topics.SingleOrDefault(t => t.Id == articlePreview.Topic);
            articlePreviewsDtos.Add(new ArticlePreviewDto
            {
                Id = articlePreview.Id,
                ArticleId = articlePreview.ArticleId,
                Title = articlePreview.Title,
                ImageUrl = articlePreview.ImageUrl,
                PreviewText = articlePreview.PreviewText,
                Date = articlePreview.Date.ToLocalTime(),
                ArticleUrl = articlePreview.ArticleUrl,
                PublicArticleId = articlePreview.PublicArticleId,
                Topic = _mapper.Map<TopicDto>(articleTopic)
            });
        }
        
        return articlePreviewsDtos.ToList();
    }
}