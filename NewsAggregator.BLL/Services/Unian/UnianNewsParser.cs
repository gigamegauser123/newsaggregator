﻿using System.Globalization;
using System.Net;
using System.Security.AccessControl;
using System.Text;
using AngleSharp;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Options;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.BLL.Services.Unian.Interfaces;

namespace NewsAggregator.BLL.Services.Unian;

public class UnianNewsParser : IUnianNewsParser
{
    private readonly IBrowsingContext _browsingContext;
    private readonly WebClient _webClient;
    private readonly UnianSourceOptions _sourceOptions;
    private readonly StringBuilder _stringBuilder;
    private readonly ILogger<UnianNewsParser> _logger;

    public UnianNewsParser(IBrowsingContext browsingContext, WebClient webClient,
        IOptions<UnianSourceOptions> parserOptions, ILogger<UnianNewsParser> logger)
    {
        _browsingContext = browsingContext;
        _webClient = webClient;
        _logger = logger;
        _sourceOptions = parserOptions.Value;
        _stringBuilder = new StringBuilder();
    }

    public async Task<List<ArticlePreviewDto>?> ParseArticlePreviewsAsync(
        string url,
        DateTime dateFrom,
        DateTime dateTo,
        CancellationToken cancellationToken)
    {
        try
        {
            var isFinal = false;
            var parsedArticlePreviews = new List<ArticlePreviewDto>();
            var pageCount = 1;

            while (!isFinal)
            {
                var page = await _browsingContext.OpenAsync(url + $"?page={pageCount}", cancellationToken);


                var articlePreviews = page.QuerySelectorAll(".list-thumbs__item");
                if (articlePreviews.Length > 1)
                {
                    for (var i = 0; i < articlePreviews.Length; i++)
                    {
                        var articleInfo = articlePreviews[i].QuerySelector(".list-thumbs__info");
                        var articleTitle = articleInfo?.QuerySelector("h3 a")?.TextContent.Replace("\n", "");
                        var articleUrl = articleInfo?.QuerySelector("h3 a")?.Attributes.GetNamedItem("href")?.Value;
                        var articleDate = articleInfo?.QuerySelector(".list-thumbs__time")?.TextContent;
                        const string dateFormat = "HH:mm, dd.MM.yyyy";

                        var parsedArticleDate =
                            DateTime.ParseExact(articleDate, dateFormat, CultureInfo.InvariantCulture);

                        if (parsedArticleDate > dateTo) continue;

                        if (parsedArticleDate < dateFrom)
                        {
                            isFinal = true;
                            break;
                        }

                        if (articleUrl ==
                            "https://www.unian.ua/society/kabmin-poyasniv-kerivnikam-ova-za-yakih-umov-potribno-evakuyuvati-ditey-iz-zakladiv-cilodobovogo-perebuvannya-12278202.html")
                        {
                            var a = 5;
                        }

                        var articleThumbnail = articlePreviews[i].QuerySelector(".list-thumbs__image");
                        var imageUrl = articleThumbnail?.QuerySelector("img")?.Attributes.GetNamedItem("data-src")
                            ?.Value;
                        if (pageCount == 20 && i == 17)
                        {
                            var a = 4;
                        }

                        parsedArticlePreviews.Add(new ArticlePreviewDto
                        {
                            Id = Guid.NewGuid(),
                            ArticleId = Guid.NewGuid(),
                            Title = articleTitle,
                            ImageUrl = imageUrl,
                            ArticleUrl = articleUrl,
                            Date = parsedArticleDate.ToUniversalTime()
                        });
                        _logger.LogInformation(
                            $"Parsed article preview in UnianNewsParser. Page {pageCount}, preview number {i})");
                    }

                    await Task.Delay(TimeSpan.FromMilliseconds(800), cancellationToken);
                    pageCount++;
                }
                else
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(2000), cancellationToken);
                }
            }

            return parsedArticlePreviews;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article previews error in UnianNewsParser: {e.Message}");
            return null;
        }
    }

    public async Task<ArticleDto?> ParseArticleAsync(string url, CancellationToken cancellationToken)
    {
        try
        {
            var page = await _browsingContext.OpenAsync(url, cancellationToken);

            var articleContainer = page.QuerySelector(".article");
            if (articleContainer is null) return await ParsePublicationAsync(url, cancellationToken);

            var articleTitle = articleContainer.QuerySelector("h1").TextContent;
            var articleInfo = articleContainer.QuerySelector(".article__info");
            var articleAuthor = articleInfo.QuerySelector("div .article__author--bottom .article__author-name")
                ?.TextContent
                .Replace("\n", "");
            var articleDate = articleInfo.QuerySelector("div .article__info-item")?.TextContent;

            var dateFormat = "HH:mm, dd.MM.yy";
            var parsedArticleDate = DateTime.ParseExact(articleDate, dateFormat, CultureInfo.InvariantCulture);
            var articleTextPreview = articleContainer.QuerySelector(".article__like-h2")?.TextContent;

            var articleContent = articleContainer.QuerySelector(".article-text");

            if (articleTextPreview != string.Empty) _stringBuilder.Append($"<h2>{articleTextPreview}</h2>");

            var isFinal = false;

            foreach (var articleElement in articleContent.Children)
            {
                if (isFinal) break;

                switch (articleElement)
                {
                    case IHtmlParagraphElement paragraph:
                        if (paragraph.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<p>{paragraph.TextContent}</p>");
                        }

                        break;

                    case IHtmlHeadingElement heading:
                        if (heading.TextContent == "Вас також можуть зацікавити новини:")
                        {
                            isFinal = true;
                            break;
                        }

                        if (heading.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<{heading.LocalName}>{heading.TextContent}</{heading.LocalName}>");
                        }

                        break;

                    case IHtmlElement { ClassName: "photo_block" } htmlElement:
                        var image = htmlElement.QuerySelector("img");
                        var imageUrl = image.Attributes.GetNamedItem("data-src").Value;
                        var imageAlt = image.Attributes.GetNamedItem("alt").Value;
                        var imageTitle = image.Attributes.GetNamedItem("title").Value;
                        _stringBuilder.Append($"<img alt='{imageAlt}' src='{imageUrl}' title='{imageTitle}'>");
                        break;
                }
            }

            var article = new ArticleDto
            {
                Title = articleTitle,
                HtmlBody = _stringBuilder.ToString(),
                Author = articleAuthor,
                Date = parsedArticleDate.ToUniversalTime(),
                SourceUrl = url
            };

            _logger.LogInformation($"Parsed article in UnianNewsParser. Article url: {url}");

            _stringBuilder.Clear();

            return article;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing article error in UnianNewsParser (Article url: {url}) {e.Message}");
            return null;
        }
    }

    private async Task<ArticleDto?> ParsePublicationAsync(string url, CancellationToken cancellationToken)
    {
        try
        {
            var page = await _browsingContext.OpenAsync(url, cancellationToken);

            var publicationContainer = page.QuerySelector(".publication");

            var publicationTitle = publicationContainer?.QuerySelector("h1")?.TextContent.Replace("\n", "");
            var publicationInfo = publicationContainer?.QuerySelector(".publication__info");
            var publicationAuthor = publicationContainer?.QuerySelector(".publication__left-info")
                ?.QuerySelector(".publication__author")?.QuerySelector("a")?.TextContent.Replace("\n", "");
            var publicationDate = publicationInfo?.QuerySelector(".time")?.TextContent;
            var mainImageElement = publicationContainer?.QuerySelector(".publication__main-image");
            var mainImage = mainImageElement?.QuerySelector("img");
            var mainImageUrl = mainImage?.Attributes.GetNamedItem("src")?.Value;
            var mainImageAlt = mainImage?.Attributes.GetNamedItem("alt")?.Value;
            var mainImageTitle = mainImage?.Attributes.GetNamedItem("title")?.Value;

            _stringBuilder.Append($"<img alt='{mainImageAlt}' src='{mainImageUrl}' title='{mainImageTitle}'>");


            var dateFormat = "HH:mm, dd.MM.yyyy";
            var parsedPublicationDate = DateTime.ParseExact(publicationDate, dateFormat, CultureInfo.InvariantCulture);

            var publicationContent = publicationContainer?.QuerySelector(".publication-text");

            var isFinal = false;

            foreach (var articleElement in publicationContent?.Children)
            {
                if (isFinal) break;

                switch (articleElement)
                {
                    case IHtmlParagraphElement paragraph:
                        if (paragraph.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<p>{paragraph.TextContent}</p>");
                        }

                        break;
                    case IHtmlHeadingElement heading:
                        if (heading.TextContent == "Вас також можуть зацікавити новини:")
                        {
                            isFinal = true;
                            break;
                        }

                        if (heading.TextContent != string.Empty)
                        {
                            _stringBuilder.Append($"<{heading.LocalName}>{heading.TextContent}</{heading.LocalName}>");
                        }

                        break;
                    case IHtmlElement { ClassName: "photo_block" } htmlElement:
                        var image = htmlElement.QuerySelector("img");
                        var imageUrl = image.Attributes.GetNamedItem("src").Value;
                        var imageAlt = image.Attributes.GetNamedItem("alt").Value;
                        var imageTitle = image.Attributes.GetNamedItem("title").Value;
                        _stringBuilder.Append($"<img alt='{imageAlt}' src='{imageUrl}' title='{imageTitle}'>");
                        break;
                    case IHtmlDivElement { ClassName: "publication__tags" } htmlDivElement:
                        isFinal = true;
                        break;
                }
            }

            var article = new ArticleDto
            {
                Title = publicationTitle,
                HtmlBody = _stringBuilder.ToString(),
                Author = publicationAuthor,
                Date = parsedPublicationDate.ToUniversalTime(),
                SourceUrl = url
            };
            _stringBuilder.Append($"<h3>{publicationAuthor}</h3>");

            _logger.LogInformation($"Parsed publication in UnianNewsParser. Publication url: {url}");

            _stringBuilder.Clear();

            return article;
        }
        catch (Exception e)
        {
            _logger.LogCritical($"Parsing publication error in UnianNewsParser (Publication url: {url}) {e.Message}");
            return null;
        }
    }
}