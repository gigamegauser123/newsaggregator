﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace NewsAggregator.BLL.Services.Utils;

public static class DateParser
{
    public static DateTime ParseDateTime(string input)
    {
        DateTime result;

        if (input.Contains(":") && !input.Contains(" "))
        {
            string[] timeParts = input.Split(':');
            int hour = int.Parse(timeParts[0]);
            int minute = int.Parse(timeParts[1]);

            DateTime today = DateTime.Today;
            result = new DateTime(today.Year, today.Month, today.Day, hour, minute, 0);
        }
        else if (input.Contains("вчора о"))
        {
            string[] timeParts = input.Split(' ');
            int hour = int.Parse(timeParts[timeParts.Length - 1].Split(':')[0]);
            int minute = int.Parse(timeParts[timeParts.Length - 1].Split(':')[1]);

            DateTime yesterday = DateTime.Today.AddDays(-1);
            result = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, hour, minute, 0);
        }
        else if (input.Contains(","))
        {
            const string dateFormat = "dd.MM.yyyy, HH:mm";
            result = DateTime.ParseExact(input, dateFormat, CultureInfo.InvariantCulture);
        }
        else if (input.Contains("о"))
        {
            const string dateFormat = "dd.MM.yyyy  HH:mm";
            var correctInput = Regex.Replace(input, "о", "").Replace(@"\s+", "").Trim();

            result = DateTime.ParseExact(correctInput, dateFormat, CultureInfo.InvariantCulture);
        }
        else
        {
            const string dateFormat = "dd.MM.yyyy HH:mm";
            result = DateTime.ParseExact(input, dateFormat, CultureInfo.InvariantCulture);
        }

        return result;
    }
}