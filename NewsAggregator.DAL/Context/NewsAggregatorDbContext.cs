﻿using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities;

namespace NewsAggregator.DAL.Context;

public sealed class NewsAggregatorDbContext : DbContext
{
    public DbSet<NewsWebsite> NewsWebsites { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Topic> Topics { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<ArticlePreview> ArticlePreviews { get; set; }
    public DbSet<TopicNewsWebsite> TopicsNewsWebsites { get; set; }
    
    public NewsAggregatorDbContext(DbContextOptions<NewsAggregatorDbContext> options) : base(options)
    {
       Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        
        
        modelBuilder.Entity<NewsWebsite>().HasData(
            new NewsWebsite
            {
                Id = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "Unian",
                BaseUrl = "https://www.unian.ua"
            }
        );
        
        modelBuilder.Entity<NewsWebsite>().HasData(
            new NewsWebsite
            {
                Id = Guid.Parse("6988FB32-AA1B-47CC-829B-7FEE25FF1862"),
                Name = "ITC",
                BaseUrl = "https://itc.ua/"
            }
        );
        
        modelBuilder.Entity<NewsWebsite>().HasData(
            new NewsWebsite
            {
                Id = Guid.Parse("A98C05D3-9AD8-48D9-B5FC-BFBEF364E405"),
                Name = "Liga.net",
                BaseUrl = "https://www.liga.net/ua/"
            }
        );
        
        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("87A89B6F-6B0B-4EDD-AFBB-93F787240FFC"),
                Name = "Ukraine",
                LocalizedName = "Україна"
            }
        );
        
        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("23d40b08-1c6c-4304-95d9-9f92de33d1e3"),
                Name = "Politics",
                LocalizedName = "Політика"
            }
        );

        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("30f03513-069b-448b-893f-aa33447f9c74"),
                Name = "War",
                LocalizedName = "Війна"
            }
        );

        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("fa0126ca-7325-43af-8b99-4e44938a31c6"),
                Name = "Economy",
                LocalizedName = "Економіка"
            }
        );

        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("02b6b58a-8313-49b2-b002-b68487ed64c0"),
                Name = "World",
                LocalizedName = "Світ"
            }
        );

        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("938f960a-42e8-40de-bcd6-02da4cc18408"),
                Name = "Technology",
                LocalizedName = "Технології"
            }
        );

        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("637a6296-399b-4d4a-8663-83c9c1f5a34d"),
                Name = "Science",
                LocalizedName = "Наука"
            }
        );
        
        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("5fc1bec6-0314-4e36-b659-cc5a6804fb33"),
                Name = "Software",
                LocalizedName = "Софт"
            }
        );
        
        modelBuilder.Entity<Topic>().HasData(
            new Topic
            {
                Id = Guid.Parse("e40902ed-f5a5-4dc4-b011-d33d2f7cc469"),
                Name = "Bank",
                LocalizedName = "Банки"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("8DAEDFDD-7175-479A-8AC4-C15CC0728F8F"),
                TopicId = Guid.Parse("87A89B6F-6B0B-4EDD-AFBB-93F787240FFC"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "society",
                Url = "https://www.unian.ua/society"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("cb72383f-daee-4f8f-a87c-2e66dcecf091"),
                TopicId = Guid.Parse("23d40b08-1c6c-4304-95d9-9f92de33d1e3"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "politics",
                Url = "https://www.unian.ua/politics"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("5245c0b3-99ab-463e-a881-63ad44e69785"),
                TopicId = Guid.Parse("30f03513-069b-448b-893f-aa33447f9c74"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "war",
                Url = "https://www.unian.ua/war"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("1e29bb4e-cd1f-403e-ac9c-7629309c76f5"),
                TopicId = Guid.Parse("fa0126ca-7325-43af-8b99-4e44938a31c6"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "economy",
                Url = "https://www.unian.ua/detail/all_news/economics"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("426a8eb2-1847-4e8a-bd9e-40df177c46ed"),
                TopicId = Guid.Parse("938f960a-42e8-40de-bcd6-02da4cc18408"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "techno",
                Url = "https://www.unian.ua/techno"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("a0d6719c-1790-4d52-bb22-f40be6968901"),
                TopicId = Guid.Parse("637a6296-399b-4d4a-8663-83c9c1f5a34d"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "science",
                Url = "https://www.unian.ua/science"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("b9a0f14e-adba-4f5e-80d4-8bb8c11b5e35"),
                TopicId = Guid.Parse("02b6b58a-8313-49b2-b002-b68487ed64c0"),
                NewsWebsiteId = Guid.Parse("623924EA-3993-43F5-9ADC-A7A0A2C45044"),
                Name = "world",
                Url = "https://www.unian.ua/world"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("5f5c06f1-dc57-41d6-bb2e-0b07d331da64"),
                TopicId = Guid.Parse("23d40b08-1c6c-4304-95d9-9f92de33d1e3"),
                NewsWebsiteId = Guid.Parse("A98C05D3-9AD8-48D9-B5FC-BFBEF364E405"),
                Name = "politics",
                Url = "https://news.liga.net/ua/politics"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("606e0154-175f-4f31-a22f-0df2768302be"),
                TopicId = Guid.Parse("02b6b58a-8313-49b2-b002-b68487ed64c0"),
                NewsWebsiteId = Guid.Parse("A98C05D3-9AD8-48D9-B5FC-BFBEF364E405"),
                Name = "world",
                Url = "https://news.liga.net/ua/world"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("6944f319-4a67-41ce-a138-33f9bf7f0455"),
                TopicId = Guid.Parse("fa0126ca-7325-43af-8b99-4e44938a31c6"),
                NewsWebsiteId = Guid.Parse("A98C05D3-9AD8-48D9-B5FC-BFBEF364E405"),
                Name = "ekonomika",
                Url = "https://finance.liga.net/ua/ekonomika"
            }
        );
        
       
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("9f3e4cf5-f213-457e-bafb-dfaf645bdb75"),
                TopicId = Guid.Parse("e40902ed-f5a5-4dc4-b011-d33d2f7cc469"),
                NewsWebsiteId = Guid.Parse("A98C05D3-9AD8-48D9-B5FC-BFBEF364E405"),
                Name = "Bank",
                Url = "https://finance.liga.net/ua/bank"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("6C17F927-0E93-42D6-B520-846C4188B7FE"),
                TopicId = Guid.Parse("87A89B6F-6B0B-4EDD-AFBB-93F787240FFC"),
                NewsWebsiteId = Guid.Parse("6988FB32-AA1B-47CC-829B-7FEE25FF1862"),
                Name = "ukrayina",
                Url = "https://itc.ua/ua/ukrayina/"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("619ca5e0-8616-4bfb-8387-39dd7a8e1780"),
                TopicId = Guid.Parse("637a6296-399b-4d4a-8663-83c9c1f5a34d"),
                NewsWebsiteId = Guid.Parse("6988FB32-AA1B-47CC-829B-7FEE25FF1862"),
                Name = "nauka-ta-kosmos",
                Url = "https://itc.ua/ua/nauka-ta-kosmos/"
            }
        );
        
        modelBuilder.Entity<TopicNewsWebsite>().HasData(
            new TopicNewsWebsite()
            {
                Id = Guid.Parse("a2e56726-b702-4788-97da-110f30e1b4e9"),
                TopicId = Guid.Parse("5fc1bec6-0314-4e36-b659-cc5a6804fb33"),
                NewsWebsiteId = Guid.Parse("6988FB32-AA1B-47CC-829B-7FEE25FF1862"),
                Name = "soft",
                Url = "https://itc.ua/ua/soft/"
            }
        );
    }
}