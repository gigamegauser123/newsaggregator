﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class Article : BaseEntity
{
    public string Title { get; set; } = null!;
    public string HtmlBody { get; set; } = null!;
    public string? Author { get; set; } = null!;
    public DateTime Date { get; set; } 
    public Guid PublicId { get; set; }
    public Guid NewsWebsite { get; set; }
    public Guid Topic { get; set; }
    public string SourceUrl { get; set; } = null!;
}