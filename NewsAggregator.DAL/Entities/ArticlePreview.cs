﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class ArticlePreview : BaseEntity
{
    public Guid ArticleId { get; set; }
    public string Title { get; set; } = null!;
    public string? ImageUrl { get; set; } 
    public string? PreviewText { get; set; } 
    public Guid PublicArticleId { get; set; }
    public Guid Topic { get; set; }
    public string ArticleUrl { get; set; } = null!;
    public DateTime Date { get; set; }
}