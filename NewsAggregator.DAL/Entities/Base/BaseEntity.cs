﻿namespace NewsAggregator.DAL.Entities.Base;

public abstract class BaseEntity
{
    public Guid Id { get; set; }
}