﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class NewsWebsite : BaseEntity
{
    public string Name { get; set; } = null!;
    public string BaseUrl { get; set; } = null!;
}