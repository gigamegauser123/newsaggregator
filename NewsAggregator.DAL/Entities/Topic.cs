﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class Topic : BaseEntity
{
    public string Name { get; set; } = null!;
    public string LocalizedName { get; set; } = null!;
}