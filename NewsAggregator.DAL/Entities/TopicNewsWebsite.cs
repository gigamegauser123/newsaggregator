﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class TopicNewsWebsite : BaseEntity
{
    public Guid TopicId { get; set; }
    public Guid NewsWebsiteId { get; set; }
    public string Name { get; set; } = null!;
    public string Url { get; set; } = null!;
}