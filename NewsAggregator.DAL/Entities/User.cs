﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Entities;

public class User : BaseEntity
{
    public string Username { get; set; } = null!;
    public string HashedPassword { get; set; } = null!;
}