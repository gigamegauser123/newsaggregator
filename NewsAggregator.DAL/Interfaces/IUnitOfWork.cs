﻿using Microsoft.EntityFrameworkCore;

namespace NewsAggregator.DAL.Interfaces;

public interface IUnitOfWork<TContext> 
    where TContext : DbContext
{
    void Commit();
    Task CommitAsync(CancellationToken cancellationToken);
}