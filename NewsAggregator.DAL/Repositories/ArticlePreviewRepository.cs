﻿using System.Linq.Expressions;
using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Base;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.DAL.Repositories;

public class ArticlePreviewRepository : GenericRepository<ArticlePreview, NewsAggregatorDbContext>, IArticlePreviewRepository
{
    private readonly NewsAggregatorDbContext _context;
    
    public ArticlePreviewRepository(NewsAggregatorDbContext context) : base(context)
    {
        _context = context;
    }

    public async Task<IEnumerable<ArticlePreview>?> GetPaginatedListAsync(int pageSize, int pageNumber, CancellationToken cancellationToken, Topic? topic = null)
    {
        var query = _context.ArticlePreviews.AsQueryable();

        if (topic != null)
        {
            query = query.Where(p => p.Topic == topic.Id);
        }

        var totalCount = query.Count();
        if (totalCount == 0) return null;
        var pageCount = (int)Math.Ceiling((double)totalCount / pageSize);

        pageNumber = Math.Max(pageNumber, 1);
        pageNumber = Math.Min(pageNumber, pageCount);

        var articlePreviews = query
            .OrderByDescending(s => s.Date)
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .ToList();

        return articlePreviews;
    }

    public async Task<int> GetRecordsCountAsync(CancellationToken cancellationToken, Expression<Func<ArticlePreview, bool>>? predicate = null)
    {
        var records = _context.ArticlePreviews.AsQueryable();
        if (predicate != null) records = records.Where(predicate);

        var recordsCount = records.Count();

        return recordsCount;
    }

    public async Task<IEnumerable<ArticlePreview>> GetLastArticlePreviewAsync(int count, CancellationToken cancellationToken)
    {
        var articlePreviews = _context.ArticlePreviews
            .OrderByDescending(a => a.Date)
            .ToList() 
            .GroupBy(a => a.ArticleUrl)
            .Select(g => g.First())
            .Take(count)
            .ToList();
        
        return articlePreviews;
    }
}