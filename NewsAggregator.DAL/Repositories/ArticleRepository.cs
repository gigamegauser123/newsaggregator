﻿using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Base;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.DAL.Repositories;

public class ArticleRepository : GenericRepository<Article, NewsAggregatorDbContext>, IArticleRepository
{
    private readonly NewsAggregatorDbContext _context;
    
    public ArticleRepository(NewsAggregatorDbContext context) : base(context)
    {
        _context = context;
    }
}