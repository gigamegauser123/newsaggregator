﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities.Base;
using NewsAggregator.DAL.Repositories.Interfaces.Base;

namespace NewsAggregator.DAL.Repositories.Base;

public class GenericRepository<TEntity, TContext> : IGenericRepository<TEntity, TContext>
    where TEntity : BaseEntity
    where TContext : DbContext
{
    protected TContext Context;
    private readonly DbSet<TEntity> _entities;

    public GenericRepository(TContext context)
    {
        Context = context;
        _entities = Context.Set<TEntity>();
    }

    public void Add(TEntity entity)
    {
        _entities.Add(entity);
    }

    public async Task AddAsync(TEntity entity, CancellationToken cancellationToken)
    {
        await _entities.AddAsync(entity, cancellationToken);
    }

    public void AddRange(IEnumerable<TEntity> entities)
    {
        _entities.AddRange(entities);
    }

    public async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
    {
        await _entities.AddRangeAsync(entities, cancellationToken);
    }

    public void Remove(TEntity entity)
    {
        _entities.Remove(entity);
    }

    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        _entities.RemoveRange(entities);
    }

    public void Update(TEntity entity)
    {
        if (entity is null) throw new ArgumentException(nameof(entity));

        Context.Entry(entity).State = EntityState.Modified;
    }

    public TEntity? Get(Guid id)
    {
        return _entities.Find(id);
    }

    public async Task<TEntity?> GetAsync(Guid id, CancellationToken cancellationToken)
    {
        return await _entities.FindAsync(id, cancellationToken);
    }

    public IEnumerable<TEntity>? GetAll()
    {
        return _entities.ToList();
    }

    public async Task<IEnumerable<TEntity>?> GetAllAsync(CancellationToken cancellationToken)
    {
        return await _entities.ToListAsync(cancellationToken);
    }

    public IEnumerable<TEntity>? Find(Expression<Func<TEntity, bool>> predicate)
    {
        return _entities.Where(predicate);
    }

    public async Task<IEnumerable<TEntity>?> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken)
    {
        return await _entities.Where(predicate).ToListAsync(cancellationToken);
    }

    public TEntity? SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
    {
        return _entities.SingleOrDefault(predicate);
    }

    public async Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken)
    {
        return await _entities.SingleOrDefaultAsync(predicate, cancellationToken);
    }

    public IEnumerable<TEntity> GetAllAsNoTracking()
    {
        return _entities.AsNoTracking();
    }
}