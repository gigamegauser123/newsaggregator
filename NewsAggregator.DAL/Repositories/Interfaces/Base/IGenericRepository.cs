﻿using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces.Base;

public interface IGenericRepository<TEntity, TContext> 
    : IUpdateRepository<TEntity>, IReadOnlyRepository<TEntity> 
    where TEntity : BaseEntity
    where TContext : DbContext
{
    IEnumerable<TEntity> GetAllAsNoTracking();
}