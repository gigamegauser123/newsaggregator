﻿using System.Linq.Expressions;
using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces.Base;

public interface IReadOnlyRepository<TEntity> where TEntity : BaseEntity
{
    TEntity? Get(Guid id);
    Task<TEntity?> GetAsync(Guid id, CancellationToken cancellationToken);
    
    IEnumerable<TEntity>? GetAll();
    Task<IEnumerable<TEntity>?> GetAllAsync(CancellationToken cancellationToken);
    
    IEnumerable<TEntity>? Find(Expression<Func<TEntity, bool>> predicate);
    Task<IEnumerable<TEntity>?> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken);

    TEntity? SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
    Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken);
}