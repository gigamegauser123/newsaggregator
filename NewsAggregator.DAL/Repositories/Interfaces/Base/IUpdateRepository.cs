﻿using NewsAggregator.DAL.Entities.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces.Base;

public interface IUpdateRepository<TEntity> where TEntity : BaseEntity
{
    void Add(TEntity entity);
    Task AddAsync(TEntity entity, CancellationToken cancellationToken);
    
    void AddRange(IEnumerable<TEntity> entities);
    Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken);
    
    void Remove(TEntity entity);
    void RemoveRange(IEnumerable<TEntity> entities);
    
    void Update(TEntity entity);
}