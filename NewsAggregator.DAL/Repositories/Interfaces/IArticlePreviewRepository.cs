﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Interfaces.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces;

public interface IArticlePreviewRepository : IGenericRepository<ArticlePreview, DbContext>
{
    Task<IEnumerable<ArticlePreview>?> GetPaginatedListAsync(int pageSize, int pageNumber, CancellationToken cancellationToken, Topic? topic = null);
    Task<int> GetRecordsCountAsync(CancellationToken cancellationToken, Expression<Func<ArticlePreview, bool>>? predicate = null);
    Task<IEnumerable<ArticlePreview>> GetLastArticlePreviewAsync(int count, CancellationToken cancellationToken);
}