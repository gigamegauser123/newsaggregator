﻿using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Interfaces.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces;

public interface ITopicNewsWebsiteRepository : IGenericRepository<TopicNewsWebsite, DbContext>
{
    
}