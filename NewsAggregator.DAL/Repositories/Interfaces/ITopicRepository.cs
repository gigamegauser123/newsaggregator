﻿using Microsoft.EntityFrameworkCore;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Interfaces.Base;

namespace NewsAggregator.DAL.Repositories.Interfaces;

public interface ITopicRepository : IGenericRepository<Topic, DbContext>
{
    
}