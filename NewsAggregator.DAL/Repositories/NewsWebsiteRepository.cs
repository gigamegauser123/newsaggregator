﻿using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Base;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.DAL.Repositories;

public class NewsWebsiteRepository : GenericRepository<NewsWebsite, NewsAggregatorDbContext>, INewsWebsiteRepository
{
    private readonly NewsAggregatorDbContext _context;
    
    public NewsWebsiteRepository(NewsAggregatorDbContext context) : base(context)
    {
        _context = context;
    }
}