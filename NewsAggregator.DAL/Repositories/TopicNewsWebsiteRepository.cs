﻿using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Base;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.DAL.Repositories;

public class TopicNewsWebsiteRepository : GenericRepository<TopicNewsWebsite, NewsAggregatorDbContext>, ITopicNewsWebsiteRepository
{
    private readonly NewsAggregatorDbContext _context;
    
    public TopicNewsWebsiteRepository(NewsAggregatorDbContext context) : base(context)
    {
        _context = context;
    }
}