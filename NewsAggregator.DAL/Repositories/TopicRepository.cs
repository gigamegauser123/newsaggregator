﻿using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Entities;
using NewsAggregator.DAL.Repositories.Base;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator.DAL.Repositories;

public class TopicRepository : GenericRepository<Topic, NewsAggregatorDbContext>, ITopicRepository
{
    private readonly NewsAggregatorDbContext _context;
    
    public TopicRepository(NewsAggregatorDbContext context) : base(context)
    {
        _context = context;
    }
}