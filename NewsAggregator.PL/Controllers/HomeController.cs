﻿using System.Diagnostics;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;
using NewsAggregator.BLL.DTOs;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.BLL.Services.Unian;
using NewsAggregator.BLL.Services.Unian.Interfaces;
using NewsAggregator.Filters;
using NewsAggregator.Models;

namespace NewsAggregator.Controllers;

public class HomeController : Controller
{
    private readonly INewsService _newsService;
    private readonly ILogger<HomeController> _logger;
    private readonly IContentAggregator _contentAggregator;
    
    public HomeController(IContentAggregator contentAggregator, ILogger<HomeController> logger, INewsService newsService)
    {
        _contentAggregator = contentAggregator;
        _logger = logger;
        _newsService = newsService;
    }

    public async Task<IActionResult> Index(CancellationToken cancellationToken)
    {
        var lastArticlePreviews = await _newsService.GetLastArticlePreviewAsync(24, cancellationToken);
        
        var ukraineArticlePreviews = await _newsService.GetArticlePreviewsAsync(cancellationToken, "ukraine",
            1, 4);
        
        var warArticlePreviews = await _newsService.GetArticlePreviewsAsync(cancellationToken, "war",
            1, 4);
        
        var technologyArticlePreviews = await _newsService.GetArticlePreviewsAsync(cancellationToken, "technology",
            1, 4);

        ViewBag.LastArticlePreviews = lastArticlePreviews;
        ViewBag.UkraineArticlePreviews = ukraineArticlePreviews;
        ViewBag.WarArticlePreviews = warArticlePreviews;
        ViewBag.TechnologyArticlePreviews = technologyArticlePreviews;

        return View();
    }
    
    [Route("/{topic}")]
    public async Task<IActionResult> ArticlePreviews(string topic, PaginationFilter paginationFilter, CancellationToken cancellationToken)
    {
        try
        {
            var articlePreviews = await _newsService.GetArticlePreviewsAsync(cancellationToken, topic,
                paginationFilter.PageNumber, paginationFilter.PageSize);

            var topicDto = await _newsService.GetTopicAsync(topic, cancellationToken);

            if (topicDto is null || !articlePreviews.Any() || articlePreviews is null) return NotFound();
        
        
            var recordsCount = await _newsService.GetRecordsCountAsync(cancellationToken, p => p.Topic == topicDto.Id);
            var pageCount = (int)Math.Ceiling((double) recordsCount / paginationFilter.PageSize); 
            var lastArticlePreviews = await _newsService.GetLastArticlePreviewAsync(10, cancellationToken);

            ViewBag.LastArticlePreviews = lastArticlePreviews;
            ViewBag.PageCount = pageCount;
            ViewBag.PageNumber = paginationFilter.PageNumber;
            ViewBag.PageSize = paginationFilter.PageSize;
            ViewBag.Topic = topicDto;
            return View(articlePreviews);
        }
        catch (Exception e)
        {
            return NotFound();
        }
    }

    [Route("/{topic}/{publicArticleId}")]
    public async Task<IActionResult> GetArticle(string topic, Guid publicArticleId, CancellationToken cancellationToken)
    {
        try
        {
            var topicDto = await _newsService.GetTopicAsync(topic, cancellationToken);

            if (topicDto is null) return NotFound();

            var articleDto = await _newsService.GetArticleAsync(topicDto, publicArticleId, cancellationToken);

            if (articleDto is null) return NotFound();

            var lastArticlePreview = await _newsService.GetLastArticlePreviewAsync(10, cancellationToken);

            ViewBag.LastArticlePreview = lastArticlePreview;
            return View("Article", articleDto);

        }
        catch (Exception e)
        {
            return NotFound();
        }
        
        return NotFound();
    }
}
















