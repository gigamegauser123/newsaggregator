﻿using System.Net;
using AngleSharp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NewsAggregator.BLL.Options;
using NewsAggregator.BLL.Services;
using NewsAggregator.BLL.Services.Interfaces;
using NewsAggregator.BLL.Services.ITC;
using NewsAggregator.BLL.Services.ITC.Interfaces;
using NewsAggregator.BLL.Services.LigaNet;
using NewsAggregator.BLL.Services.LigaNet.Interfaces;
using NewsAggregator.BLL.Services.Unian;
using NewsAggregator.BLL.Services.Unian.Interfaces;
using NewsAggregator.DAL;
using NewsAggregator.DAL.Context;
using NewsAggregator.DAL.Interfaces;
using NewsAggregator.DAL.Repositories;
using NewsAggregator.DAL.Repositories.Interfaces;

namespace NewsAggregator;

public static class RegisterDependentServices
{
    public static WebApplicationBuilder RegisterServices(this WebApplicationBuilder builder)
    {
        builder.Services.Configure<UnianSourceOptions>(builder.Configuration.GetSection("UnianSourceOptions"));
        builder.Services.Configure<LigaNetSourceOptions>(builder.Configuration.GetSection("LigaNetSourceOptions"));
        builder.Services.Configure<ItcSourceOptions>(builder.Configuration.GetSection("ItcSourceOptions"));

        builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();
        builder.Services.AddAutoMapper(config =>
        {
            config.AddMaps(typeof(NewsAggregator.BLL.AssemblyReference));
            config.AddMaps(typeof(NewsAggregator.AssemblyReference));
        });

        builder.Services.AddDbContext<NewsAggregatorDbContext>(
            o => o.UseNpgsql(builder.Configuration.GetConnectionString("NewsAggregatorDb"))
        );

        var dbContext = builder.Services.BuildServiceProvider().GetService<NewsAggregatorDbContext>();
        dbContext.Database.Migrate();

        builder.Services.AddTransient<WebClient>();
        builder.Services.AddTransient<IArticleRepository, ArticleRepository>();
        builder.Services.AddTransient<IArticlePreviewRepository, ArticlePreviewRepository>();
        builder.Services.AddTransient<INewsWebsiteRepository, NewsWebsiteRepository>();
        builder.Services.AddTransient<ITopicRepository, TopicRepository>();
        builder.Services.AddTransient<ITopicNewsWebsiteRepository, TopicNewsWebsiteRepository>();
        builder.Services.AddScoped<IUnianNewsParser, UnianNewsParser>();
        builder.Services.AddScoped<INewsSource, UnianNewsSource<IUnianNewsParser>>();
        builder.Services.AddScoped<ILigaNetNewsParser, LigaNetNewsParser>();
        builder.Services.AddScoped<INewsSource, LigaNetNewsSource<ILigaNetNewsParser>>();
        builder.Services.AddScoped<IItcNewsParser, ItcNewsParser>();
        builder.Services.AddScoped<INewsSource, ItcNewsSource<IItcNewsParser>>();
        builder.Services.AddScoped<IContentAggregator, ContentAggregator>();
        builder.Services.AddScoped<INewsService, NewsService>();
        builder.Services.AddScoped<IUnitOfWork<NewsAggregatorDbContext>, UnitOfWork<NewsAggregatorDbContext>>();
        builder.Services.AddHostedService<BackgroundNewsAggregator>();
        
        builder.Services.AddScoped(_ =>
        {
            var config = Configuration.Default.WithDefaultLoader();
            return BrowsingContext.New(config);
        });
        
        return builder;
    }
}